//
const addInputBtn = document.querySelector("#addInput");
const showResultBtn_1 = document.querySelector("#showResult_1");
const showResultBtn_2 = document.querySelector("#showResult_2");
const showResultBtn_3 = document.querySelector("#showResult_3");
const showResultBtn_4 = document.querySelector("#showResult_4");
const showResultBtn_5 = document.querySelector("#showResult_5");
const showResultBtn_6 = document.querySelector("#showResult_6");
const showResultBtn_7 = document.querySelector("#showResult_7");
const showResultBtn_8 = document.querySelector("#showResult_8");
const showResultBtn_9 = document.querySelector("#showResult_9");
const showResultBtn_10 = document.querySelector("#showResult_10");
const input_arr = [];

// Thêm số vào mảng
addInputBtn.addEventListener("click", addInput);
// Hiển thị câu 1
showResultBtn_1.addEventListener("click", function () {
  document.querySelector(
    "#result_1"
  ).value = `Tổng số dương: ${sumOfPositiveNumber(input_arr)}`;
});
// Hiển thị câu 2
showResultBtn_2.addEventListener("click", function () {
  document.querySelector("#result_2").value = `Số Dương: ${countPositiveNumber(
    input_arr
  )}`;
});
// Hiển thị câu 3
showResultBtn_3.addEventListener("click", function () {
  document.querySelector("#result_3").value = `Số nhỏ nhất: ${findMin(
    input_arr
  )}`;
});
// Hiển thị câu 4
showResultBtn_4.addEventListener("click", function () {
  document.querySelector("#result_4").value = findMinPositiveNumber(input_arr);
});
// Hiển thị câu 5
showResultBtn_5.addEventListener("click", function () {
  document.querySelector("#result_5").value = findLastEvenNumber(input_arr);
});
// Hiển thị câu 6
showResultBtn_6.addEventListener("click", function () {
  const pos_1_Val = document.querySelector("#txt-pos-1").value * 1;
  const pos_2_Val = document.querySelector("#txt-pos-2").value * 1;
  document.querySelector("#result_6").value = `${swapNumberPosition(
    input_arr,
    pos_1_Val,
    pos_2_Val
  )}`;
});
// Hiển thị câu 7
showResultBtn_7.addEventListener("click", function () {
  document.querySelector(
    "#result_7"
  ).value = `Mảng sau khi sắp xếp: ${input_arr.sort((a, b) => a - b)}`;
});
// Hiển thị câu 8
showResultBtn_8.addEventListener("click", function () {
  document.querySelector("#result_8").value = findFirstPrimeNumber(input_arr);
});
// Hiển thị câu 9
showResultBtn_9.addEventListener("click", function () {
  document.querySelector("#result_9").value = countInt(input_arr);
});
// Hiển thị câu 10
showResultBtn_10.addEventListener("click", function () {
  document.querySelector("#result_10").value = comparePosAndNegNumInArray(
    input_arr,
    countPositiveNumber,
    countNegativeNumber
  );
});

function addInput() {
  const input = document.querySelector("#input-num").value * 1;
  const output_arr = document.querySelector("#output-array");
  input_arr.push(input);
  output_arr.value = `${input_arr}`;
  document.querySelector("#input-num").value = "";
}
function sumOfPositiveNumber(input_arr) {
  let sum = 0;
  input_arr.forEach((input) => {
    if (input > 0) {
      sum += input;
    }
  });
  return sum;
}
function countPositiveNumber(input_arr) {
  let count = 0;
  input_arr.forEach((input) => {
    if (input > 0) {
      count++;
    }
  });
  return count;
}
function findMin(input_arr) {
  let min = input_arr[0];
  input_arr.forEach((input) => {
    if (input <= min) {
      min = input;
    }
  });
  return min;
}
function findMinPositiveNumber(input_arr) {
  let pos_arr = [];
  input_arr.forEach((input) => {
    if (input > 0) {
      pos_arr.push(input);
    }
  });
  let min = pos_arr[0];
  if (pos_arr.length === 0) {
    return "Không có số dương trong mảng";
  } else {
    pos_arr.forEach((num) => {
      if (num <= min) {
        min = num;
      }
    });
  }
  return `Số dương nhỏ nhất: ${min}`;
}
function findLastEvenNumber(input_arr) {
  let even_int_arr = [];
  input_arr.forEach((element) => {
    if (element % 2 === 0) {
      even_int_arr.push(element);
    }
  });
  if (even_int_arr.length === 0) {
    return "Không có số chẵn trong mảng";
  } else {
    return `Số Chẵn cuối cùng: ${even_int_arr[even_int_arr.length - 1]}`;
  }
}
function swapNumberPosition(input_arr, fromIndex, toIndex) {
  let i = input_arr[toIndex];
  input_arr[toIndex] = input_arr[fromIndex];
  input_arr[fromIndex] = i;
  return input_arr;
}
function isPrime(num) {
  if (Number.isInteger(num) == false || num < 2) {
    return false;
  } else {
    for (let x = 2; x < num; x++) {
      if (num % x === 0) {
        return false;
      }
    }
    return true;
  }
}
function findFirstPrimeNumber(input_arr) {
  const firstPrime = input_arr.find((element) => isPrime(element) === true);
  return firstPrime;
}
function countInt(input_arr) {
  let count = 0;
  input_arr.forEach((element) => {
    if (Number.isInteger(element) === true) {
      count++;
    }
  });
  return count;
}
function countNegativeNumber(input_arr) {
  let count = 0;
  input_arr.forEach((element) => {
    if (element < 0) {
      count++;
    }
  });
  return count;
}
function comparePosAndNegNumInArray(input_arr, countPosNum, countNegNum) {
  let countPos = countPosNum(input_arr);
  let countNeg = countNegNum(input_arr);
  if (countPos > countNeg) {
    return (result = `Số Dương > Số Âm`);
  } else if (countNeg > countPos) {
    return (result = `Số Âm > Số Dương`);
  } else {
    return (result = `Số Dương = Số Âm`);
  }
}
